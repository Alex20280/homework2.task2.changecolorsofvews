package layouts.sourceit.com.homework2task2changecolorsofviews;

import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView textview1;
    TextView textview2;
    TextView textview3;
    TextView textview4;
    ArrayList<Integer> color = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        textview1 = (TextView) findViewById(R.id.textview1);
        textview2 = (TextView) findViewById(R.id.textview2);
        textview3 = (TextView) findViewById(R.id.textview3);
        textview4 = (TextView) findViewById(R.id.textview4);

        color.add(Color.BLACK);
        color.add(Color.BLUE);
        color.add(Color.RED);
        color.add(Color.GREEN);
        color.add(Color.YELLOW);
        color.add(Color.WHITE);
        color.add(Color.MAGENTA);
        color.add(Color.CYAN);
        color.add(Color.DKGRAY);

        textview1.setOnClickListener(this);
        textview2.setOnClickListener(this);
        textview3.setOnClickListener(this);
        textview4.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.textview1:
                textview2.setBackgroundColor(Color.BLACK);
                textview3.setBackgroundColor(Color.BLUE);
                textview4.setBackgroundColor(Color.RED);
                break;
            case R.id.textview2:
                textview1.setBackgroundColor(Color.GREEN);
                textview3.setBackgroundColor(Color.YELLOW);
                textview4.setBackgroundColor(Color.WHITE);
                break;
            case R.id.textview3:
                textview1.setBackgroundColor(Color.MAGENTA);
                textview2.setBackgroundColor(Color.CYAN);
                textview4.setBackgroundColor(Color.DKGRAY);
                break;
            case R.id.textview4:
                textview1.setBackgroundColor(Color.YELLOW);
                textview2.setBackgroundColor(Color.CYAN);
                textview3.setBackgroundColor(Color.GREEN);
                break;
        }

    }
}
